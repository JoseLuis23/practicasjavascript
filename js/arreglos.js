/* // Declaración de arreglos
var arreglo = [19, 20, 3, 2, 6, 3, 3, 9];

// Mostrar los elementos de arreglos
function mostrarArreglo() {
    for (con = 0; con < arreglo.length; ++con) {
        console.log(con + ":" + arreglo[con]);
    }
}
// Función que regrese el promedio de los elementos del arreglo
function promArreglo() {
    let prom = 0;
    for (con = 0; con < arreglo.length; ++con) {
        prom = prom + arreglo[con];
    }
    prom = prom / arreglo.length;
    return prom;
}

/* Realizar una función que regrese un valor entero que 
represente la cantidad de números pares en el arreglo 
function numPar() {
    let par = 0;
    for (con = 0; con < arreglo.length; ++con) {
        if (arreglo[con] % 2 == 0) {
            par++;
        } else {
            console.log("no es par");
        }
    }
    return par;
}

// Realizar una función que te regrese el valor mayor contenido en el arreglo
function numMayor() {
    let mayor = 0;
    for (con = 0; con < arreglo.length; ++con) {

        if (mayor <= arreglo[con]) {
            mayor = arreglo[con];
        } else {
            console.log("No es mayor que el anterior");
        }
    }
    return mayor;
}

// Llamamos la función para mostrar el arreglo
mostrarArreglo();
// Llamamos la función para mostrar el promedio del arreglo
const prom = promArreglo();
console.log("El Promedio del Arreglo es: " + prom);
// Llamamos la funcion paara los numero pares
const par = numPar();
console.log("EL total de números pares es: " + par);
// Llamamos la funcion para el número mayor
const mayor = numMayor();
console.log("El Número mayor es: " + mayor);

/* Diseñe una función que recibe un valor numerico  que indica la cantidad de elementos del arreglo
El arreglo debera de llenarse con valores aleatorias en rango de 0 al 1000
posteriormente mostrara el arreglo */

// Capturamos un número  */


/*function numRandom(tamArreglo) {
    const cmbAleatorios = document.getElementById('cmbNumero');
    let array = []; //Arreglo Vacio

    for (con = 0; con < tamArreglo; ++con) {
        array[con] = Math.floor(Math.random() * 1000);
        
        let option = document.createElement('option');
        option.value = array[con];
        option.innerHTML = arr[con];
        cmbAleatorios.appendChild(option);
    }
}*/

// Llamamos la funcion
//numRandom(tamArreglo);//

var arreglo = [];
const btnGenerar = document.getElementById('btnGenerar');
const btnLimpiar = document.getElementById('btnLimpiar');
function mostrarArreglo(arreglo) {
    for (con = 0; con < arreglo.length; ++con) {
        console.log(con + ":" + arreglo[con]);
    }
}

function promArreglo(arreglo) {
    let prom = 0;
    for (con = 0; con < arreglo.length; ++con) {
        prom = prom + arreglo[con];
        posicion = con;
    }
    prom = prom / arreglo.length;
    document.getElementById('prom').textContent = prom.toFixed(2);
    return prom;
}

function numMayor(arreglo) {
    let mayor = 0;
    let posicion = 0;
    for (con = 0; con < arreglo.length; ++con) {
        if (mayor <= arreglo[con]) {
            mayor = arreglo[con];
            posicion = con + 1;
        }
    }
    document.getElementById('may').textContent = `${mayor} en posición ${posicion}`;
    return mayor;
}

function numMenor(arreglo) {
    
    if (arreglo.length === 0) {
        throw new Error("El arreglo está vacío");
    }
    let posicion = 0;
    let menor = arreglo[0];
    for (let i = 1; i < arreglo.length; i++) {
        if (arreglo[i] < menor) {
            menor = arreglo[i];
            posicion = 1 + i;
        }
    }
    document.getElementById('men').textContent = `${menor} en posición ${posicion}`;
    return menor;
}

function simetrico(arreglo) {
    if (!Array.isArray(arreglo)) {
        throw new Error("El argumento no es un arreglo.");
    }
    var si = "Si";
    var no = "No";
    const longitud = arreglo.length;
    for (let i = 0; i < longitud / 2; i++) {
        if (arreglo[i] !== arreglo[longitud - 1 - i]) {
            return false;
        }
    }
    return true;
}

function pares(arreglo) {
    if (!Array.isArray(arreglo)) {
        throw new Error("El argumento no es un arreglo.");
    }
    if (arreglo.length === 0) {
        throw new Error("El arreglo está vacío.");
    }
    let numerosPares = 0;
    for (let i = 0; i < arreglo.length; i++) {
        if (arreglo[i] % 2 === 0) {
            numerosPares++;
        }
    }
    const porcentaje = (numerosPares / arreglo.length) * 100;
    document.getElementById('par').textContent = `${porcentaje.toFixed(2)}%`;
    return porcentaje;
}

function impares(arreglo) {
    if (!Array.isArray(arreglo)) {
        throw new Error("El argumento no es un arreglo.");
    }
    if (arreglo.length === 0) {
        throw new Error("El arreglo está vacío.");
    }
    let numerosImpares = 0;
    for (let i = 0; i < arreglo.length; i++) {
        if (arreglo[i] % 2 !== 0) {
            numerosImpares++;
        }
    }
    const porcentaje = (numerosImpares / arreglo.length) * 100;
    document.getElementById('impar').textContent = `${porcentaje.toFixed(2)}%`;
    return porcentaje;
}

let array = [];
function numRandom(tamArreglo) {
    const cmbAleatorios = document.getElementById('cmbNumeros');
    for (con = 0; con < tamArreglo; ++con) {
        array[con] = Math.floor(Math.random() * 1000);
        let option = document.createElement('option');
        option.value = array[con];
        option.innerHTML = array[con];
        cmbAleatorios.appendChild(option);
    }
}

btnGenerar.addEventListener('click', function () {
    let tamArreglo = parseInt(document.getElementById('cantidad').value);
    numRandom(tamArreglo);
    mostrarArreglo(array);
    promArreglo(array);
    numMayor(array);
    numMenor(array);
    impares(array);
    pares(array);
    simetrico(array);
    const resultado = simetrico(array);
    if (resultado) {
        document.getElementById('sim').textContent = "SI";
    } else {
        document.getElementById('sim').textContent = "NO";
    }
});
btnLimpiar.addEventListener('click', function () {
    document.getElementById('cantidad').value = '';
    document.getElementById('prom').textContent = ''; 
    document.getElementById('may').textContent = '';
    document.getElementById('men').textContent = '';
    document.getElementById('par').textContent = '';
    document.getElementById('impar').textContent = '';
    document.getElementById('sim').textContent = '';
    array=[];
    while (cmbNumeros.firstChild) {
        cmbNumeros.removeChild(cmbNumeros.firstChild);
      }
});