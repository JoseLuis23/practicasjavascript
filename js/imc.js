// Obtener el objeto del botón de calcular
const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click', function () {
    let Altura = document.getElementById('txtAltura').value;
    let Peso = document.getElementById('txtPeso').value;

    // HACER LOS CALCULOS
    let altura = Altura * Altura;
    let res = (Peso / altura);
    
    // Mostrar el IMC
    document.getElementById('txtResultado').value = res.toFixed(2);
    

// MOSTRAR LA IMAGEN
    let genero = document.querySelector('input[name="genero"]:checked').value;
    let imagen = document.getElementById("Imagen");

    if (genero === "hombre") {
        if (res < 18.5) {
            imagen.src = "/img/01H.png";
        } else if (res <= 24.9) {
            imagen.src = "/img/02H.png";
        } else if (res <= 29.9) {
            imagen.src = "/img/03H.png";
        } else if (res <= 34.9) {
            imagen.src = "/img/04H.png";
        } else if (res <= 39.9) {
            imagen.src = "/img/05H.png";
        } else if (res >= 40) {
            imagen.src = "/img/06H.png";
        }
    } else if (genero === "mujer") {
        if (res < 18.5) {
            imagen.src = "/img/01M.png";
        } else if (res <= 24.9) {
            imagen.src = "/img/02M.png";
        } else if (res <= 29.9) {
            imagen.src = "/img/03M.png";
        } else if (res <= 34.9) {
            imagen.src = "/img/04M.png";
        } else if (res <= 39.9) {
            imagen.src = "/img/05M.png";
        } else if (res >= 40) {
            imagen.src = "/img/06M.png";
        }
    }

// Edad y calorías
    let Edad = document.getElementById('txtEdad').value;
    let Cal = 0;
    if (genero === "hombre") {
        if (Edad < 18) {
            Cal = (17.686 * Peso) + 692.2;
        } else if (Edad > 18) {
            Cal = (15.057 * Peso) + 692.2;
        } else if (Edad > 30) {
            Cal = (11.472 * Peso) + 873.1;
        } else if (Edad > 60) {
            Cal = (11.711 * Peso) + 587.7;
        }
        document.getElementById('txtCalorias').value = Cal.toFixed(2);
    } else if (genero === "mujer") {
        if (Edad < 18) {
            Cal = (13.384 * Peso) + 692.6;
        } else if (Edad > 18) {
            Cal = (14.818 * Peso) + 486.6;
        } else if (Edad > 30) {
            Cal = (8.126 * Peso) + 845.6;
        } else if (Edad > 60) {
            Cal = (9.082 * Peso) + 658.5;
        }
        document.getElementById('txtCalorias').value = Cal.toFixed(2);
    }
});

// CODIFICAR EL BOTÓN DE LIMPIAR
const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function () {
    document.getElementById('txtAltura').value = '';
    document.getElementById('txtPeso').value = '';
    document.getElementById('txtResultado').value = '';
    document.getElementById('txtEdad').value = '';
    document.getElementById('txtCalorias').value = '';
});
